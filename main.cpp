#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <unistd.h>
#include "rsa.h"

void log(std::string message) {
    std::cout << "[INFO]: " << message << std::endl;
}
void error(std::string message) {
    std::cerr << "[ERROR]: " << message << std::endl;
}
std::string receiveMessage(int socket) {
    int max_len = 20000;
    // Initialization of char array with max length defined in server configuration
    char message[max_len];
    // Setting all chars in array to 0
    memset(message, 0, max_len);

    // Reading message from client
    int result = (int) read(socket, &message, max_len - 1);

    std::string message_str;

    // If result is less than zero -> error while getting message
    if (result < 0) {
        error("Chyba při příjmání zprávy.");
        return "";
    }

        // If result equals to zero -> client dropped -> closes client's socket
    else if (result == 0) {
        error("Ztraceno spojení s klientem, končím aplikaci.");
        close(socket);
        log("--- Končím server ---");
        exit(1);
    }

        // Else message handling
    else {
        int i = 0;

        // While char at index i is not ';' add char into messageString
        while (message[i] != ';' && i < max_len && message[i] != '\0') {
            message_str += message[i];
            i++;
        }
        if (message[i] == ';') {
            message_str += message[i];
            return message_str;
        }

    }

}
int sendMessage(int socket, std::string message) {
    const char *msgChar = message.c_str();
    int result = send(socket, (void *) msgChar, message.length(), 0);
    return result;
}

void console_thread(int socket, rsa::public_key pub_key){
    while(true){
        char msg[2000];
        std::cin.getline(msg, sizeof(msg));

        rsa r;
        std::string message_enc = r.encrypt(std::string(msg), pub_key);
        sendMessage(socket, "msg:" + message_enc + ";");

        log("Posílám zašifrovanou zprávu: " + message_enc);
    }
}

int main() {
    rsa::private_key priv_key;
    rsa::public_key pub_key;

    rsa r;
    r.generate_keys(pub_key, priv_key);

    ///////////// SERVER PART //////////////////
    int m_socket;
    int m_port;
    struct sockaddr_in addr;

    log("--- Spouštím server ---");

    m_port = 20001;
    m_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    int optval = 1;
    setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(m_port);
    addr.sin_addr.s_addr = inet_addr("127.0.0.1");

    if (bind(m_socket, (sockaddr *) &addr, sizeof(addr)) < 0) {
        error("Chyba při bindování socketu.");
        log("--- Končím server ---");
        exit(1);
    }

    log("Socket nabindován.");

    if (listen(m_socket, 1) < 0) {
        error("Chyba při poslouchání socketu.");
        log("--- Končím server ---");
        exit(1);
    }

    log("Socket naslouchá vpořádku na adrese 127.0.0.1:" + std::to_string(m_port));

    sockaddr_in m_incommingAddr;
    unsigned int m_incommingAddrLength = sizeof(m_incommingAddr);
    int m_incomming_socket;
    m_incomming_socket = accept(m_socket, (struct sockaddr *) &m_incommingAddr, &m_incommingAddrLength);

    log("Klient se připojil k serveru. IP " + std::string(inet_ntoa(m_incommingAddr.sin_addr)));

    log("Posílám klientovi veřejný klíč. n=" + std::to_string(pub_key.n) + ", e=" + std::to_string(pub_key.e));
    sendMessage(m_incomming_socket, "add-key:" + std::to_string(pub_key.n) + ":" + std::to_string(pub_key.e) + ";");

    rsa::public_key client_key;
    bool thread_running = false;
    while (true) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));

        std::string message = receiveMessage(m_incomming_socket);
        message.at(message.length() - 1) = ' ';

        if (message.find(":") != std::string::npos) {
            std::vector<std::string> split = util::split(message, ":");

            if (split.at(0) == "add-key" && split.size() == 3) {
                log("Nastavuji veřejný klíč klienta. n=" + split[1] + ", e=" + split[2]);
                client_key.n = atol(split[1].c_str());
                client_key.e = atol(split[2].c_str());

                if(!thread_running) {
                    log("Spouštím odesílací vlákno, pro odeslání napiště něco do konzole a odentrujte.");
                    std::thread t(console_thread, m_incomming_socket, client_key);
                    t.detach();
                    thread_running = true;
                }
            } else if (split.at(0) == "msg" && split.size() == 2) {
                log("Přijata zašifrovaná zpráva:" + split[1]);
                std::string message_dec = r.decrypt(split.at(1), priv_key);
                log("Zpráva rozšifrována: " + message_dec);
            }
        }
    }
}